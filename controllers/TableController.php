<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Cell;

class TableController extends Controller {
	
	public function actionIndex() {
		$displayCells = [];
		$cells = Cell::find()->all();
		foreach ($cells as $displayCell) {
			$displayCells[$displayCell->x.":".$displayCell->y] = $displayCell->value;
		}
		if (Yii::$app->request->post() && Yii::$app->request->post("cells")) {
			$cellsPost = Yii::$app->request->post("cells");
			$validated = [];
			$changed = [];
			$validateFail = false;
			foreach ($cellsPost as $cellPos => $value) {
				$cellPosArray = explode(":", $cellPos);
				$x = $cellPosArray[0];
				$y = $cellPosArray[1];
				$cell = new Cell();
				$cell->x = $x;
				$cell->y = $y;
				$cell->value = $value;
				if ($cell->validate()) {
					$validated[] = [$x, $y, $value];
					$changed[$cellPos] = ["value" => $value, "validated" => true];
				} else {
					$validateFail = true;
					$changed[$cellPos] = ["value" => $value, "validated" => false];
				}
			}
			if (!$validateFail) {
				Cell::saveMass($validated);
				$this->goHome();
			} else {
				return $this->render('index', ["cells" => $displayCells, "changed" => $changed]);
			}
		} else {
			return $this->render('index', ["cells" => $displayCells]);
		}
    }
	
}