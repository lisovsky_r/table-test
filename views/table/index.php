<?php
$this->title = 'Table App';
?>

<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>
<table class="tablecell">
	<thead>
		<tr>
			<th></th>
			<?php for ($i = 1; $i <= 100; $i++): ?>
				<th><?php echo $i; ?></th>
			<?php endfor; ?>
		</tr>
	</thead>
	<tbody>
		<?php for ($x = 1; $x <= 100; $x++): ?>
			<tr>
				<td class="row-number"><?php echo $x; ?></td>
				<?php for ($y = 1; $y <= 100; $y++): ?>
					<td class="cell-inner <?php echo (isset($changed[$x.":".$y]) && !$changed[$x.":".$y]["validated"]) ? "fail-validate" : "" ?>" data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>">
						<?php if(isset($changed[$x.":".$y])): ?>
							<input type="text"  data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>" value="<?php echo $changed[$x.":".$y]["value"] ?>" name="cells[<?php echo $x; ?>:<?php echo $y; ?>]">
						<?php else: ?>
							<?php echo $cells[$x.":".$y] ?? ""; ?>
						<?php endif ?>	
					</td>
				<?php endfor; ?>
			</tr>
		<?php endfor; ?>
	</tbody>
</table>
<input type="submit" class="btn btn-info update-table" value="Update">
<?php ActiveForm::end(); ?>