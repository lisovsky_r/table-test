<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class Cell extends ActiveRecord {

	public static function tableName()
    {
        return 'cells';
    }
	
    public function rules()
    {
        return [
            [['x', 'y'], 'integer', 'min' => 1, 'max' => 100],
            ['value', 'integer', 'min' => 1, 'max' => 99999],
        ];
    }
	
	public static function saveMass($cellsData) {
		$db = Yii::$app->db;
		$sql = $db->queryBuilder->batchInsert('cells', ['x', 'y', 'value'], $cellsData);
		$db->createCommand($sql . ' ON DUPLICATE KEY UPDATE value = VALUES(value)')->execute();
	}
	
}