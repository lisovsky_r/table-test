$(".tablecell").on("click", ".cell-inner", function() {
	if (!$(this).has("input").length) {
		var oldVal = $(this).html().trim();
		var x = $(this).data("x");
		var y = $(this).data("y");
		var input = $("<input type='text' name='cells["+x+":"+y+"]' value='"+oldVal+"'>");
		$(this).empty();
		$(this).append(input);
		input.focus();
	}
});

$(".tablecell").on("keypress", ".cell-inner input", function(e) {
	var value = $(this).val();
    if (value.length >= 5) {
		e.preventDefault();
	}
});

$(".tablecell").on("change", ".cell-inner input", function() {
	if ($(this).parent().hasClass("fail-validate")) {
		$(this).parent().removeClass("fail-validate");
	}
});

$(document).ready(function() {
	$("table").css("width", $(window).width());
	$("thead").css("width", $(window).width());
	$("tbody").css("width", $(window).width());
	$("tbody").css("height", $(window).height() - 20);
  $('tbody').scroll(function(e) { 
    $('thead').css("left", -$("tbody").scrollLeft());
    $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()-5); 
    $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()-5); 
  });
});
