<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cells`.
 */
class m180628_085351_create_cells_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('cells', [
			'x' => $this->integer()->notNull(),
			'y' => $this->integer()->notNull(),
			'value' => $this->string()->notNull(),
        ]);
		$this->addPrimaryKey('cells_pk', 'cells', ['x', 'y']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cells');
    }
}
